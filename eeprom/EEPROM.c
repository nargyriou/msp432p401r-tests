#include <ti/devices/msp432p4xx/driverlib/driverlib.h>

#define CS GPIO_PORT_P4, GPIO_PIN5
#define CLOCK GPIO_PORT_P4, GPIO_PIN2
#define SO GPIO_PORT_P5, GPIO_PIN4
#define SI GPIO_PORT_P5, GPIO_PIN5
#define WP GPIO_PORT_P4, GPIO_PIN4

#define OP_WREN  0b00000110
#define OP_WRDI  0b00000100
#define OP_RDSR  0b00000101
#define OP_WRSR  0b00000001
#define OP_READ  0b00000011
#define OP_WRITE 0b00000010
#define MAX_PAR_PAGE 32

#define MAX_CHAR 4096

#define LED  GPIO_PORT_P1, GPIO_PIN0
#define T 2

void clock_high() {
  GPIO_setOutputHighOnPin(CLOCK); 
  GPIO_setOutputHighOnPin(LED);  
}

void clock_low() {
  GPIO_setOutputLowOnPin(CLOCK); 
  GPIO_setOutputLowOnPin(LED);   
}

bool send_bit(bool b) {
  if (b) {
    // Serial.print('1');
    GPIO_setOutputHighOnPin(SI);
  } else {
    // Serial.print('0');
    GPIO_setOutputLowOnPin(SI);
  }

  clock_high();

  bool ret = GPIO_getInputPinValue(SO);
  
  clock_low();

  // Serial.print(ret);

  return ret;
}

char send_char(char in) {
  char output = 0;

  for (int i=0; i < 8; i++) {
    output = output << 1;
    output |= send_bit( (in & (0b10000000) ) ); // read first bit
    in = in << 1; // get next bit; throw out first bit
  }

  Serial.print(output);
  return output;
}

int read_string(uint16_t addr, char* output_str) {
  // Activate read
  GPIO_setOutputLowOnPin(CS);

  char opcode = OP_READ;
  send_char(opcode);

  // char high_part = addr >> 8;
  // char low_part = addr & 0x0F;

  // en fait ça c'est mieux
  // (copyright)
  char* tmp = (char*) ((void*) (&addr));
  
  send_char(tmp[0]);
  send_char(tmp[1]);

  int i = 0;
  int fuse = MAX_CHAR;
  char read_char;
  do {
    read_char = send_char(0x0);
    output_str[i++] = read_char;

    fuse--;
  } while (read_char != '\0' && fuse > 0);

  GPIO_setOutputHighOnPin(CS);

  return 0;
}

void write_page(uint16_t addr, char* input_str) {
  GPIO_setOutputLowOnPin(CS);
  send_char(OP_WREN);
  GPIO_setOutputHighOnPin(CS);
  delay(T/2);

  GPIO_setOutputLowOnPin(CS);
  send_char(OP_WRITE);

  char* tmp = (char*) ((void*) (&addr));
  send_char(tmp[0]);
  send_char(tmp[1]);
  
  int fuse = MAX_PAR_PAGE;
  int i = 0;
  do {
    send_char(input_str[i]);
  } while(input_str[i++] != '/0' && --fuse);

  GPIO_setOutputHighOnPin(CS);
  
  send_char(OP_WRDI);
}

void setup()
{
    GPIO_setAsInputPinWithPullUpResistor(SO);

    GPIO_setAsOutputPin(CS);
    GPIO_setAsOutputPin(CLOCK);
    GPIO_setAsOutputPin(SI);
    GPIO_setAsOutputPin(LED);

    GPIO_setOutputHighOnPin(CS);

    Serial.begin(115200);

    delay(500);

    while (!Serial) {
      // wait for serial port to connect. Needed for native USB
    }

    char buffer[MAX_CHAR + 1];
    buffer[MAX_CHAR] = '\0';

    char chaine[MAX_PAR_PAGE] = "Nicolas & Clovis";
    Serial.println("ECRITURE___________________________________");    
    write_page(0x00, chaine);
    
    Serial.println("LECTURE___________________________________");    
    read_string(0x00, buffer);

    Serial.println(buffer);
}

void loop()
{
  
}

