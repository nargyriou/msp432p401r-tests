Taille en octets: 4096 octets
On a besoin de 6 pattes;

* Vcc pour l'alimentation,
* Vss pour la terre,
* Chip Select (CS) pour activer la puce,
* Serial Clock (SCK) pour pouvoir communiquer avec la puce,
* SI pour lui demander une lecture,
* SO pour que nous puissions recevoir les données

La puce accepte une horloge d'au maximum 10MHz donc il faut envoyer des signaux haut/bas de 50 nano secondes chaque
Deux délais de 50 nanosecondes
Un emplacement mémoire fait 8 bits
Un opcode fait 8 bits avec 3 bits utiles selon la doc
12 bits permettent d'accéder à tous les 4096 octets
16 bits sont nécessaires pour l'adresse selon le protocole
Si la communication est possible c'est que le CS est low
L'EEPROM transmet ses données sur le front descendant, les données sont disponibles sur le SO sur le front montant
MSB
Il est possible de lire le contenu de plusieurs adresses consécutives après une seule commande READ. Les adresses s'incrémentent lorsqu'on arrive à la fin d'un bloc. Lorsqu'on arrive à la fin de la mémoire en revanche, l'adresse revient à 0. Nous n'avons pas d'information indiquant que nous avons atteint la fin de la mémoire.
On ne peut écrire que 32 octets consécutifs en mémoire, après quoi l'adresse d'écriture boucle sur l'adresse initiale et nous écrasons les premières données écrites