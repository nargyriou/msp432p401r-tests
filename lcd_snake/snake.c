#define HX8353E
#include "Screen_HX8353E.h" 

Screen_HX8353E myScreen; 

#define NONE  0
#define UP    1
#define LEFT  2
#define DOWN  3
#define RIGHT 4

const int xpin = 2;                  // x-axis of the accelerometer
const int ypin = 26;                  // y-axis
const int btnPin = 32;
int score = 0;
char buff[12];
int sSize = 0;
bool last[128][128];
short analogX = 0;
short analogY = 0;
short x = 0;
short y = 0;
char snake_dir = UP;
int i = 0;


void reset() {
  x = sSize/2;
  y = x;

  for(char i=0; i<128; i++){
    for(char j=0; j<128; j++){
        last[i][j] = false;
    }
  }

  myScreen.clear();

  while (digitalRead(btnPin) == HIGH){
    delay(50);
  }
}

void setup() { 
  myScreen.begin(); 
  myScreen.setOrientation(0);
  sSize = myScreen.screenSizeX();
  
  Serial.begin(9600);
  pinMode(btnPin, INPUT);
  digitalWrite(btnPin, LOW);
  myScreen.setFontSize(2); 

  reset();
} 

bool collision_check(short x, short y) {
  return last[x][y];
}

char calcul_direction(short x, short y) {
  if (abs(x) > abs(y)) {
    // axis is X
    if (x > -100 && x < 100) {
      return NONE;
    }

    if (x > 100) 
      return RIGHT;
    else
      return LEFT;
  }
  else {
    // axis is Y
    if (y > -100 && y < 100) {
      return NONE;
    }

    if (y > 100) 
      return UP;
    else
      return DOWN;
  } 
}

bool move(char old_dir, char wanted_dir, short* x, short* y) { 
  if (*x < 0 || *y < 0 || *x >= 128 || *y >= 128){
    return false;
  }
  
  switch (wanted_dir) {
    case UP:
      if (old_dir != DOWN){
        *y = *y-1;
        return true;
      }
      break;
    case DOWN:
      if (old_dir != UP){
        *y = *y+1;
        return true;
      }
      break;
    case LEFT:
      if (old_dir != RIGHT){
        *x = *x-1;
        return true;
      }
      break;
    case RIGHT:
      if (old_dir != LEFT){
        *x = *x+1;
        return true;
      }
      break;
  }

  // else

  return move(old_dir, old_dir, x, y);
}

void loop() { 
  char old_dir = snake_dir;

    // if (digitalRead(btnPin) == LOW) {
      analogX = analogRead(xpin) - 500;
      analogY = analogRead(ypin) - 500;
      
      char d = calcul_direction(analogX, analogY);
  
      Serial.println((int)d);
      
      if (d > NONE) {
        snake_dir = d;
      }

  
  if (move(old_dir, snake_dir, &x, &y) && collision_check(x, y) == false){
    myScreen.point(x, y, whiteColour);
    last[x][y] = true;
    score++;
  } else {
    //todo: print game over
    myScreen.gText(20, 20, "Score", redColour);
    myScreen.gText(100, 20, itoa(score, buff, 10), redColour);
    delay(1000);
    reset();
  }
  
  delay(100);
}
