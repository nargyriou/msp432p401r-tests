#include <ti/devices/msp432p4xx/driverlib/driverlib.h>
#include "types.h"

#define IN_A GPIO_PORT_P5, GPIO_PIN5
#define IN_B GPIO_PORT_P5, GPIO_PIN4
#define LED  GPIO_PORT_P1, GPIO_PIN0

void setup()
{
    GPIO_setAsInputPinWithPullUpResistor(IN_A);
    GPIO_setAsInputPinWithPullUpResistor(IN_B);

    GPIO_setAsOutputPin(LED);

    Serial.begin(115200);
}

/**
 * @brief
 * Lit la valeur des pines P.1 et P5.5 et les interprète comme une rotation d'un codeur rotatif
 * 
 * @return
 *  Renvoie LEFT, RIGHT si une rotation a été terminée
 *  NONE si une rotation est en cours ou qu'aucune ne se fait actuellement
 */
direction determine_direction() {
  // Etat courant que renvoie le codeur; 11, 01, 10, 00
  uint8_t cur = 0;

  // Etat de notre automate fini; voir types.h
  static rotation_state state = NEUTRAL;

  // Valeur de retour
  direction codeur_dir = NONE;

  // Fusionne les bits en une seule variable
  cur  = GPIO_getInputPinValue(IN_A);
  cur |= GPIO_getInputPinValue(IN_B) << 1;

  // 4 états pour tourner à droite
  if (cur == 0b10 && state == NEUTRAL) {
    state = RIGHT_1;
  } else if (cur == 0b00 && state == RIGHT_1) {
    state = RIGHT_2;
  } else if (cur == 0b01 && state == RIGHT_2) {
    state = RIGHT_3;
  }

  // 4 états pour tourner à gauche
  else if (cur == 0b01 && state == NEUTRAL) {
    state = LEFT_1;
  } else if (cur == 0b00 && state == LEFT_1) {
    state = LEFT_2;
  } else if (cur == 0b10 && state == LEFT_2) {
    state = LEFT_3;
  }

  // Retour en position neutre
  else if (cur == 0b11) {
    // Si on retourne en position neutre et qu'on était en phase finale de notre rotation
    if (state == RIGHT_3) {
      // On renvoie une rotation terminée
      codeur_dir = RIGHT;
    } else if (state == LEFT_3) {
      codeur_dir = LEFT;
    }

    // Position neutre implique dans tous les cas un retour à l'état initial
    // /!\ C'est l'état, pas la valeur de retour
    state = NEUTRAL;
  }

  // Renvoie LEFT, RIGHT si une rotation a été terminée
  // NONE si une rotation est en cours ou qu'aucune ne se fait actuellement
  return codeur_dir;
}

void loop()
{
  unsigned int timer = 0;           // 
  unsigned int last_timer = timer;  // 
  int cnt = 0;                      // Position du codeur rotatif
  int incr;                         // Selon la vitesse du codeur, peut valoir 1, 10 ou 100. Incrémente le compteur.
  direction d;                      // Direction du compteur rotatif (LEFT, RIGHT, NONE)

  // Main loop
  for (;;) {
    // ~1 Hz selon la fréquence de l'horloge (8MHz) et le temps pour exécuter notre boucle 
    if (timer % 80000 == 0) {
      GPIO_toggleOutputOnPin(LED);

      // On peut faire ça car 80000 est divisible par 5000, 500, 10 et 1
      timer = 0;
    }
    
    d = determine_direction();

    // Calcul de vitesse
    // De combien incrémente-on le compteur ?
    switch (d) {
      case RIGHT:
      case LEFT:
        int diff = timer - last_timer;
        if (diff < 500) {
          incr = 100;
        } else if (diff < 5000) {
          incr = 10;
        } else {
          incr = 1;
        }

        last_timer = timer;
        break;
    }

    // Incrémente le compteur; print sa valeur
    switch(d){
      case RIGHT :
        cnt += incr;
        Serial.print("RIGHT ");
        Serial.println(cnt);
        
        break;
      case LEFT :
        cnt -= incr;
        Serial.print("LEFT ");
        Serial.println(cnt);

        break;
    }
    
    timer++;
  }
}

