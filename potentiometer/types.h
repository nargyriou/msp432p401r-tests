enum e_rotation_state {
  LEFT_1,
  LEFT_2,
  LEFT_3,
  RIGHT_1,
  RIGHT_2,
  RIGHT_3,
  NEUTRAL
};

enum e_direction {
  LEFT,
  RIGHT,
  NONE
};

typedef e_rotation_state rotation_state;
typedef e_direction direction;