#include <ti/devices/msp432p4xx/driverlib/driverlib.h>

#define PORT_BTN1 GPIO_PORT_P5
#define PIN_BTN1  GPIO_PIN1
#define PORT_BTN2 GPIO_PORT_P3
#define PIN_BTN2  GPIO_PIN1

void setup()
{
    volatile uint32_t i;

    // Stop watchdog timer
    WDT_A_hold(WDT_A_BASE);

    GPIO_setAsOutputPin(
        GPIO_PORT_P1,
        GPIO_PIN0
    );

    GPIO_setAsInputPinWithPullUpResistor(PORT_BTN1, PIN_BTN1);

    Serial.begin(9600);
    char buff[20];

    while(1)
    {
      GPIO_toggleOutputOnPin(GPIO_PORT_P1,GPIO_PIN0);
      itoa(GPIO_getInputPinValue(PORT_BTN1, PIN_BTN1), buff, 10);
      Serial.println( buff );

      // attente active selon instructions du prof
      if (GPIO_getInputPinValue(PORT_BTN1, PIN_BTN1) == 0) {
        for(i=100000; i>0; i--);
      } else {
        for(i=1000000; i>0; i--);
      }
    }

}

void loop()
{

}
