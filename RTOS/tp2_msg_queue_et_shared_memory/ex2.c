#include <unistd.h>
#include <mqueue.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>

/*
      \.  -   -  .                                          ,.  _~-.,           
     '          _ , -`.                                  ~'`_ \/,_. \_
   '        _,'     _,'                                 / ,"_>@`,__`~.)         
  '      ,-'      _/           ,                        | |  @@@@'  ",! .       
 '    ,-' \     _/       __,,,,,,                       |/   ^^@     .!  \      
'   ,'     \  _'        ////é è                         `' .^^^     ,'    '     
'  '       _\'          \\\'  >                          .^^^   .          \    
' ,    _,-'  \    _______ ) _^                          .^^^       '  .     \   
\,_,--'%%%%%%%\   \\__ __/ /_\                .,.,.     ^^^             ` .   .,
%%%%%%%%%%%%%%%\   \\+/  \ /  \&&&&&&&&&&&&&&&&&&&&&,  ,^^^^.  . ._ ..__ _  .'  
                \   \\| '    ' \%%%%%%%%%%%%%%%%%%%%%%%^^^^^^%%&&;_,.-=~'`^`'~=-
        RIP      \   \\ /\      \%%%%%%%%%%%%%%&&&&&%%%%%%%%%%%%%%%%%%&&;,.-=~'`
                  \   \\ \___`-.________&&&&&&&%%%%%&&&&&&&&&&&%%%%&&&_,.;^`'~=-
   Ci gissent les  \   \\__,( \_____  - \%%%%%%%%%%%%%%&&&&&&&&&-=~'`^`'~=-.,__,
     vacances de    \   \`---\/\----), ) \%%%%%%%%%%%&&&&
   monsieur Clauss   \   ||+=+=+=+=/  /\  \   _,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,_
                      \  ||________| /\ `. \  
%%%%%%%%%%%%%%%%%%%%%%%\ ||------- )/-\\  ) \ ~`'^`'~=-.,__,.-=~'`^`'~=-.,__,.-=
                        \||      ,'/   \\  \ \       / /         '-`            

                        
                            ^ (C'est vous)
*/
#ifndef PAGESIZE
	#define PAGESIZE 4096
#endif

#define PARENT_LAND 0
#define CHILD_LAND  PAGESIZE/2

#define true 1
#define false 0
#define RESET   "\033[0m"
#define RED     "\033[31m"
#define CYAN    "\x1B[36m"

#define ERR(format, ...) {                                                     \
	fprintf(stderr, RED);                                                      \
	fprintf(stderr, "ERROR: ");                                                \
	fprintf(stderr, format, __VA_ARGS__);                                      \
	fprintf(stderr, RESET);                                                    \
	fflush(stderr);                                                            \
}                                                                              \

#define DEBUG(format, ...) {                                                   \
	fprintf(stderr, CYAN);                                                     \
	fprintf(stderr, "[DEBUG]");                                                \
	fprintf(stderr, RESET);                                                    \
	fprintf(stderr, format, __VA_ARGS__);                                      \
	fflush(stderr);                                                            \
}                                                                              \


/**
 * @brief Checks an assertion and prints the standard system primitives errors
 * @details Checks an assertion 
 * If the check fails, it tries to print the message along with a posix error 
 * message
 * 
 * If there are none, it simply prints the message alone.
 * 
 * @param R Will be compared to -1
 * @param G Message
 */
static inline void check(int expr, const char* msg) {
	if (expr == 0) {
		ERR("Assertion failed: %s", msg);
		exit(1);
	}
}

static inline void syscheck(int expr, const char* msg) {
	if ( expr == -1 ) {
		if (errno) {
			ERR("Assertion failed: %s (%s)\n",
				(char*) strerror(errno), msg);
		} else {
			ERR("Assertion failed (%s)\n", msg);
		}
		exit(1);
	}
}

int waitfor(int child) {
	// Wait for the child to actually terminate
	int status;
	pid_t ended_child;
	do {
		ended_child = waitpid(child, &status, 0);
	} while (WIFEXITED(status) != 1 && ended_child != child);

	// Print a debug diagnostic
	int t0,t1,t2,t3,t4,t5;
	DEBUG(" Child diagnostic: \n\
	WIFEXITED   => %d\n\
	WEXITSTATUS => %d\n\
	WIFSIGNALED => %d\n\
	WTERMSIG    => %d\n\
	WIFSTOPPED  => %d\n\
	WSTOPSIG    => %d\n\
",
		t0=WIFEXITED(status),
		t1=WEXITSTATUS(status),
		t2=WIFSIGNALED(status),
		t3=WTERMSIG(status),
		t4=WIFSTOPPED(status),
		t5=WSTOPSIG(status)
	);

	// Just WIFEXITED is the normal behavior.
	if (t0 && !(t1 | t2 | t3 | t4 | t5)) {
		DEBUG("  Results => OK (%d)\n", ended_child);
	}
	else {
		DEBUG("  Results => Something happened ! (%d)\n", ended_child);
	}


	return 1;
}


/*
Ecrire un programme qui partage une page mémoire entre 2 processus (PAGESIZE défini dans
limits.h, sinon 4096). Le processus père écrit une chaîne de caractère dans la zone partagée. Il créé
ensuite le processus fils qui lit la zone partagée. Celuici
écrit ensuite une autre chaîne dans la zone,
puis lance une autre image du processus (execlp). Cette nouvelle image lit la zone partagée, puis écrit
une nouvelle chaîne. Le processus père attend la fin du processus fils puis lit la zone partagée.
Provoquer un affichage de chaque chaîne écrite et lue. On doit obtenir à l'affichage :
processus père : écriture de "Message de Papa" dans SHM
processus fils : lecture de "Message de Papa" dans SHM
processus fils : écriture de "Message du fiston avant exec" dans SHM
processus fils après exec : lecture de "Message du fiston avant exec" dans SHM
processus fils après exec : écriture de "Message du fiston après exec" dans SHM
processus père : lecture de "Message du fiston après exec" dans SHM
*/

union shmeu {
	void* ptr;
	char* str;
};

int send_msg(char* ptr, const char* str, size_t size) {
	return strncpy(ptr, str, size) == ptr;
}

void be_child_2() {
	int tmp;
	int shm_fd = shm_open("/toto", O_RDWR, S_IWUSR | S_IRUSR);
	syscheck(shm_fd, "shm_open failure in child2");

	union shmeu shm;
	shm.ptr = mmap(
		NULL, 
		PAGESIZE, PROT_WRITE | PROT_READ, 
		MAP_SHARED, 
		shm_fd, 
		0
	);
	check(shm.ptr != NULL, "mmap failure in child2");

	// We check if we should actually exist ?
	const char check_string[] = "Message du fiston avant exec";
	if (strncmp(&shm.str[CHILD_LAND], check_string, sizeof(check_string)) != 0)
		exit(6);

	printf("processus fils après exec : lecture de \"%s\" dans SHM\n", 
		&shm.str[CHILD_LAND]);

	const char msg[] = "Message du fiston après exec";
	printf("processus fils après exec : écriture de \"%s\" dans SHM\n", msg);
	
	tmp = send_msg(&shm.str[CHILD_LAND], msg, sizeof(msg));
	check(tmp, "send_msg failure in child2");


	munmap(shm.ptr, PAGESIZE);
	close(shm_fd);
}

void be_a_child() {
	int tmp;

	/* Shared memory boilerplate */
	int shm_fd = shm_open("/toto", O_RDWR | O_CREAT, S_IWUSR | S_IRUSR);
	syscheck(shm_fd, "shm_open failure in child");

	union shmeu shm;
	shm.ptr = mmap(NULL, PAGESIZE, PROT_WRITE | PROT_READ, MAP_SHARED, shm_fd, 0);
	check(shm.ptr != NULL, "mmap failure in child");

	/* Behavioral code */

	// Message read
	while (shm.str[0] == '\0') {
		pause();
	}
	printf("processus fils : lecture de \"%s\" dans SHM\n", shm.str);

	// Message write
	const char msg[] = "Message du fiston avant exec";
	printf("processus fils : écriture de \"%s\" dans SHM\n", msg);
	tmp = send_msg(&shm.str[CHILD_LAND], msg, sizeof(msg));
	check(tmp, "msg can't be sent in child");

	// Forking & execlp
	pid_t ppid = getpid();
	DEBUG("[CHILD 1] I will be paused.%s\n", "");
	pid_t pid = vfork();
	// Doing a vfork() instead of a regular fork()
	// This is advised when an exec() call immediatly follows a fork
	switch (pid) {
		case 0:
			// Child
			DEBUG("[CHILD 2] I was spawned. Parent is %d\n", ppid);
			tmp = execlp("./tp2.out", "./tp2.out", NULL);
			DEBUG("ERROR: execlp (%d)\n", tmp);
			// execlp error
			exit(4);
		case -1:
			// Fork error
			exit(3);
		default
:			// Parent
			DEBUG("[CHILD 1] I have resumed. Child is %d\n", 
				pid);
			waitfor(pid);
			exit(0);
	}


	/* Shared memory post */
	// no unlink in child
	munmap(shm.ptr, PAGESIZE);

	close(shm_fd);
}

int main(int argc, char const *argv[])
{
	int tmp;

	/* Shared memory boilerplate */
	int shm_fd = shm_open("/toto", O_RDWR | O_CREAT | O_EXCL, S_IWUSR | S_IRUSR);

	// If the shared memory segment existed already, then we've launched the
	// program before 'unlink' happened
	// This normally happens during the execlp phase of the exercise
	if (shm_fd == -1 && errno == EEXIST) {
		DEBUG("[CHILD 2] execlp worked as expected%s\n", "");
		be_child_2();
		exit(0);
	}

	tmp = ftruncate(shm_fd, PAGESIZE);
	syscheck(tmp, "ftruncate failure in parent");

	union shmeu shm;
	shm.ptr = mmap(NULL, PAGESIZE, PROT_WRITE | PROT_READ, MAP_SHARED, shm_fd, 0);
	check(shm.ptr != NULL, "mmap failure in parent");

	memset(shm.ptr, 0, PAGESIZE);

	/* Behavioral code */
	const char msg1[] = "Message de Papa";
	printf("processus père : écriture de \"%s\" dans SHM\n", msg1);
	tmp = send_msg(shm.str, msg1, sizeof(msg1));
	check(tmp, "msg can't be sent in parent");

	/* Behavioral code */
	pid_t ppid = getpid();
	DEBUG("[PARENT] I (%d) was spawned.\n", ppid);

	pid_t pid = fork();
	switch (pid) {
		case 0:
			// Child
			DEBUG("[CHILD 1] I was spawned. Parent is %d\n", ppid);

			be_a_child();

			exit(0);
		case -1:
			// Fork error
			exit(2);
		default:
			// Parent

			// The parent unmaps, closes but also unlinks
			waitfor(pid);

			printf("processus père : lecture de \"%s\" dans SHM\n", 
				&shm.str[CHILD_LAND]);

			munmap(shm.ptr, PAGESIZE);

			close(shm_fd);
			shm_unlink("/toto");

			exit(0);
	}


	return 0;
}
