#include <unistd.h>
#include <mqueue.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <stdint.h>

/*
      \.  -   -  .                                          ,.  _~-.,           
     '          _ , -`.                                  ~'`_ \/,_. \_
   '        _,'     _,'                                 / ,"_>@`,__`~.)         
  '      ,-'      _/           ,                        | |  @@@@'  ",! .       
 '    ,-' \     _/       __,,,,,,                       |/   ^^@     .!  \      
'   ,'     \  _'        ////é è                         `' .^^^     ,'    '     
'  '       _\'          \\\'  >                          .^^^   .          \    
' ,    _,-'  \    _______ ) _^                          .^^^       '  .     \   
\,_,--'%%%%%%%\   \\__ __/ /_\                .,.,.     ^^^             ` .   .,
%%%%%%%%%%%%%%%\   \\+/  \ /  \&&&&&&&&&&&&&&&&&&&&&,  ,^^^^.  . ._ ..__ _  .'  
                \   \\| '    ' \%%%%%%%%%%%%%%%%%%%%%%%^^^^^^%%&&;_,.-=~'`^`'~=-
        RIP      \   \\ /\      \%%%%%%%%%%%%%%&&&&&%%%%%%%%%%%%%%%%%%&&;,.-=~'`
                  \   \\ \___`-.________&&&&&&&%%%%%&&&&&&&&&&&%%%%&&&_,.;^`'~=-
   Ci gissent les  \   \\__,( \_____  - \%%%%%%%%%%%%%%&&&&&&&&&-=~'`^`'~=-.,__,
     vacances de    \   \`---\/\----), ) \%%%%%%%%%%%&&&&
   monsieur Clauss   \   ||+=+=+=+=/  /\  \   _,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,_
                      \  ||________| /\ `. \  
%%%%%%%%%%%%%%%%%%%%%%%\ ||------- )/-\\  ) \ ~`'^`'~=-.,__,.-=~'`^`'~=-.,__,.-=
                        \||      ,'/   \\  \ \       / /         '-`            

                        
                            ^ (C'est vous)

Ecrire un programme qui créé 2 queues de messages contenant chacune un message au maximum, et
qui créé un processus fils. La taille des messages sera de 4 octets par défaut, mais pourra être spécifiée
par l'utilisateur du programme. La première queue servira à l'écriture par le processus père et à la
lecture par le processus fils. La deuxième queue servira à la lecture par le processus père et à l'écriture
par le processus fils. Les lectures et écritures devront être bloquantes en cas de queues vides ou
pleines. Sur le même modèle que le TP n°1, mesurer le nombre de messages écrits et lus par chaque
processus en une minute, ainsi que la bande passante.

Quels sont les résultats ? La bande passante varietelle
lorsque l'on modifie la taille d'un message ?

Tracer une courbe (taille/bande passante) et essayer de définir une relation entre taille de message et
bande passante.


Essai 1: 19899360/minute == 331656 messages par seconde
Essai 2: 26626022/minute == 443767 messages par seconde avec un code épuré et rapide*



	Ce ralentissement de débit de 33% est causé par ce simple 'if'

		if (priority == 0 && *readval == 0x74697571) { // "quit" in hex
			break;
		}

Résultats: https://i.imgur.com/l0Igkux.png

*/

#define MESSAGE_SIZE 512
#define SAMPLE_TIME 1

#define true 1
#define false 0
#define RESET   "\033[0m"
#define RED     "\033[31m"      /* Red */
#define ERR(format, ...) {                                                     \
	fprintf(stderr, RED);                                                      \
	fprintf(stderr, format, __VA_ARGS__);                                      \
	fprintf(stderr, RESET);                                                    \
}                                                                              \

/**
 * @brief Checks an assertion and prints the standard system primitives errors
 * @details Checks an assertion 
 * If the check fails, it tries to print the message along with a posix error 
 * message
 * 
 * If there are none, it simply prints the message alone.
 * 
 * @param R Will be compared to -1
 * @param G Message
 */

static inline void check(int expr, const char* msg) {
	if (expr == 0) {
		ERR("Assertion failed: %s", msg);
		exit(1);
	}
}

static inline void syscheck(int expr, const char* msg) {
	if ( expr == -1 ) {
		if (errno) {
			ERR("Assertion failed: %s (%s)\n",
				(char*) strerror(errno), msg);
		} else {
			ERR("Assertion failed (%s)\n", msg);
		}
		exit(1);
	}
}

#define DEBUG(format, ...) printf(format, __VA_ARGS__)

int waitfor(int child) {
	// Wait for the child to actually terminate
	int status;
	pid_t ended_child;
	do {
		ended_child = waitpid(child, &status, 0);
	} while (WIFEXITED(status) != 1 && ended_child != child);

	// Print a debug diagnostic
	int t0,t1,t2,t3,t4,t5;
	DEBUG("Child diagnostic: \n\
	WIFEXITED   => %d\n\
	WEXITSTATUS => %d\n\
	WIFSIGNALED => %d\n\
	WTERMSIG    => %d\n\
	WIFSTOPPED  => %d\n\
	WSTOPSIG    => %d\n\
",
		t0=WIFEXITED(status),
		t1=WEXITSTATUS(status),
		t2=WIFSIGNALED(status),
		t3=WTERMSIG(status),
		t4=WIFSTOPPED(status),
		t5=WSTOPSIG(status)
	);

	// Just WIFEXITED is the normal behavior.
	if (t0 && !(t1 | t2 | t3 | t4 | t5))
		DEBUG("  Results => OK (%d)\n", ended_child);
	else
		DEBUG("  Results => Something happened ! (%d)\n", ended_child);


	return 1;
}


/*
 *	Actual code
 */

int received_msg = 0;
int sent_msg = 0;
int parent_finished = 0;
struct mq_attr attr;

void handle_sigalarm(int sig)
{
	parent_finished = 1;
	return;
}

void be_a_child() {
	// Open queues
	mqd_t chan1 = mq_open("/chan1", O_RDONLY | O_CREAT, 0660, &attr);
	syscheck(chan1, "Child can't open chan1");
	mqd_t chan2 = mq_open("/chan2", O_WRONLY | O_CREAT, 0660, &attr);
	syscheck(chan1, "Child can't open chan2");


	// Prepare reads
	ssize_t read_size = 0;
	unsigned int priority = 0;
	size_t buffer_size = MESSAGE_SIZE; // => 4

	char buff[MESSAGE_SIZE+1];
	memset(buff, 0, MESSAGE_SIZE);

	uint32_t *readval = (void*) buff;

	// Read until we reach a 'quit' message with priority of 0
	while (read_size != -1) {
		read_size = mq_receive(chan1, buff, buffer_size, &priority);
		received_msg++;

		/*
		DEBUG("=> '%s' %d 0x%x)\n", 
			buff, 
			*readval, 
			*readval
		);
		//*/

		if (priority == 0 && *readval == 0x74697571) { // "quit" in hex
			break;
		}
	}

	DEBUG("Messages received: %d\n", received_msg);

	mq_close(chan1);
	mq_close(chan2);
}

void be_a_parent(int pid, uint32_t wait_time) {
	mqd_t chan1 = mq_open("/chan1", O_WRONLY | O_CREAT, 0660, &attr);
	syscheck(chan1, "Parent can't open chan1");
	mqd_t chan2 = mq_open("/chan2", O_RDONLY | O_CREAT, 0660, &attr);
	syscheck(chan1, "Parent can't open chan2");

	int tmp = alarm(wait_time);
	syscheck(tmp, "Parent alarm set up");

	parent_finished = 0;
	while (parent_finished == 0) {
		tmp = mq_send(chan1, "ayy", MESSAGE_SIZE, 1);
		if (tmp != -1) {
			sent_msg++;
		}
		// syscheck(tmp, "Error sending ayy on chan1 (parent)");
	}

	tmp = mq_send(chan1, "quit", 4, 0);
	syscheck(tmp, "Parent send quit");
	sent_msg++;
	
	DEBUG("Messages sent: %d\n", sent_msg);

	mq_close(chan1);
	mq_close(chan2);

	waitfor(pid);	

	mq_unlink("/chan1");
	mq_unlink("/chan2");
}

int main(int argc, char const *argv[])
{
	/* Signal boilerplate */
	extern void handle_sigalarm();

	// Handle SIGALRM with handle_sigalarm()
	struct sigaction sa; 
	sa.sa_handler = handle_sigalarm;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	int tmp = sigaction(SIGALRM, &sa, NULL);
	syscheck(tmp, "Sigaction for alarm");

	/* Message queues boilerplate */
	int g_mq_prio_max;
	attr.mq_flags = 0;
	attr.mq_maxmsg = 1;
	attr.mq_msgsize = MESSAGE_SIZE;
	attr.mq_curmsgs = 0;
	
	#ifndef MQ_PRIO_MAX
		g_mq_prio_max = sysconf(_SC_MQ_PRIO_MAX);
	#else
		g_mq_prio_max = MQ_PRIO_MAX;
	#endif

	/* Sanity checks */
	if (MESSAGE_SIZE < 4) {
		ERR("The maximum message size can't be under 4 bytes%s\n", "");
	}

	/* Behavioral code */
	pid_t ppid = getpid();

	pid_t pid = fork();
	switch (pid) {
		case 0:
			// Child
			DEBUG("Parent is (%d)\n", ppid);

			be_a_child();

			exit(0);
		case -1:
			// Fork error
			exit(2);
		default:
			// Parent

			be_a_parent(pid, SAMPLE_TIME);

			exit(0);
	}


	return 0;
}
