#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sched.h>
#include <signal.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stddef.h>

/*
      \.  -   -  .                                          ,.  _~-.,           
     '          _ , -`.                                  ~'`_ \/,_. \_
   '        _,'     _,'                                 / ,"_>@`,__`~.)         
  '      ,-'      _/           ,                        | |  @@@@'  ",! .       
 '    ,-' \     _/       __,,,,,,                       |/   ^^@     .!  \      
'   ,'     \  _'        ////é è                         `' .^^^     ,'    '     
'  '       _\'          \\\'  >                          .^^^   .          \    
' ,    _,-'  \    _______ ) _^                          .^^^       '  .     \   
\,_,--'%%%%%%%\   \\__ __/ /_\                .,.,.     ^^^             ` .   .,
%%%%%%%%%%%%%%%\   \\+/  \ /  \&&&&&&&&&&&&&&&&&&&&&,  ,^^^^.  . ._ ..__ _  .'  
                \   \\| '    ' \%%%%%%%%%%%%%%%%%%%%%%%^^^^^^%%&&;_,.-=~'`^`'~=-
        RIP      \   \\ /\      \%%%%%%%%%%%%%%&&&&&%%%%%%%%%%%%%%%%%%&&;,.-=~'`
                  \   \\ \___`-.________&&&&&&&%%%%%&&&&&&&&&&&%%%%&&&_,.;^`'~=-
   Ci gissent les  \   \\__,( \_____  - \%%%%%%%%%%%%%%&&&&&&&&&-=~'`^`'~=-.,__,
     vacances de    \   \`---\/\----), ) \%%%%%%%%%%%&&&&
   monsieur Clauss   \   ||+=+=+=+=/  /\  \   _,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,_
                      \  ||________| /\ `. \  
%%%%%%%%%%%%%%%%%%%%%%%\ ||------- )/-\\  ) \ ~`'^`'~=-.,__,.-=~'`^`'~=-.,__,.-=
                        \||      ,'/   \\  \ \       / /         '-`            

                        
                            ^ (C'est vous)
*/

#define PRIO 17
#define SIG1 (SIGRTMIN+1)

#define true 1
#define false 0
#define RESET   "\033[0m"
#define RED     "\033[31m"
#define CYAN    "\x1B[36m"

#define ERR(format, ...) {                                                     \
	fprintf(stderr, RED);                                                      \
	fprintf(stderr, "ERROR: ");                                                \
	fprintf(stderr, format, __VA_ARGS__);                                      \
	fprintf(stderr, "\n");                                                     \
	fprintf(stderr, RESET);                                                    \
	fflush(stderr);                                                            \
}                                                                              \

#define DEBUG(format, ...) {                                                   \
	fprintf(stderr, CYAN);                                                     \
	fprintf(stderr, "[DEBUG]");                                                \
	fprintf(stderr, RESET);                                                    \
	fprintf(stderr, format, __VA_ARGS__);                                      \
	fflush(stderr);                                                            \
}                                                                              \


/**
 * @brief Checks an assertion and prints the standard system primitives errors
 * @details Checks an assertion 
 * If the check fails, it tries to print the message along with a posix error 
 * message
 * 
 * If there are none, it simply prints the message alone.
 * 
 * @param R Will be compared to -1
 * @param G Message
 */

static inline void syscheck(int expr, const char* msg) {
	if ( expr == -1 ) {
		if (errno) {
			ERR("Assertion failed: %s (%s)\n",
				(char*) strerror(errno), msg);
		} else {
			ERR("Assertion failed (%s)\n", msg);
		}
		exit(1);
	}
}

// static inline void check(int expr, const char* msg) {
// 	if (expr == 0) {
// 		ERR("Assertion failed: %s", msg);
// 		exit(1);
// 	}
// }

int waitfor(int child) {
	// Wait for the child to actually terminate
	int status;
	pid_t ended_child;
	do {
		ended_child = waitpid(child, &status, 0);
	} while (WIFEXITED(status) != 1 && ended_child != child);

	// Print a debug diagnostic
	int t0,t1,t2,t3,t4,t5;
	DEBUG(" Child diagnostic: \n\
	WIFEXITED   => %d\n\
	WEXITSTATUS => %d\n\
	WIFSIGNALED => %d\n\
	WTERMSIG    => %d\n\
	WIFSTOPPED  => %d\n\
	WSTOPSIG    => %d\n\
",
		t0=WIFEXITED(status),
		t1=WEXITSTATUS(status),
		t2=WIFSIGNALED(status),
		t3=WTERMSIG(status),
		t4=WIFSTOPPED(status),
		t5=WSTOPSIG(status)
	);

	// Just WIFEXITED is the normal behavior.
	if (t0 && !(t1 | t2 | t3 | t4 | t5)) {
		DEBUG("  Results => OK (%d)\n", ended_child);
	}
	else {
		DEBUG("  Results => Something happened ! (%d)\n", ended_child);
	}


	return 1;
}



/*
On souhaite générer de la charge processeur pour une tâche, c’est-à-dire lui 
faire effectuer desopérations actives pendant un temps C (dont on fixe l’unité 
de temps à la ms). Pour calibrer la durée des opérations actives à 1 ms, on 
utilise une boucle while qui incrémente un compteur k pendant une durée d’1 ms. 

clock_gettime(CLOCK_REALTIME, &ref);
t = (double)ref.tv_sec*1e9 + (double)ref.tv_nsec;
k=0;
while (((double)ref.tv_sec*1e9 + (double)ref.tv_nsec)<t+1e6){
	clock_gettime(CLOCK_REALTIME, &ref);
	k++;  
}

Ainsi, comme on sait que k tours de boucles prennent 1 ms à s’exécuter, k*C 
tours de bouclesprennent alors C ms à s’exécuter. Pour que chaque tâche génère 
de la charge processeurpendant un temps prédéfini, elle doit faire appel à la 
procédure attente(C).D’autres   méthodes   de   calibrage   existent,   mais   
nous   avons   choisi   celle-ci   car   elle   estindépendante du cpu de la 
machine, qui peut varier d’un processeur à l’autre, et permet doncde bien 
respecter les 1 ms requises.On considère les tâches suivantes, auxquelles on a 
fixé les priorités comme suit: T1 > T2

T1 : C=2  Période=4  Echéance=4
T2 : C=3  Période=6  Echéance=6

Chaque tâche sera exécutée par un processus différent.a
*/


/*
 *	Actual code
 */
int tache(struct timespec *last, struct timespec *now, 
	                long int *delta_s, long int *delta_ns) {
	int tmp; 

	tmp = clock_gettime(CLOCK_REALTIME, now);
	syscheck(tmp, "clock failure in main");
	
	*delta_ns = now->tv_nsec - last->tv_nsec;
	*delta_s  = now->tv_sec  - last->tv_sec;
	*last = *now;

	return 0;
}

void handle_sig1(int sig)
{
	static struct timespec last = {0, 0};
	static struct timespec now =  {0, 0};
	static long int delta_s = 0;
	static long int delta_ns = 0;

	if (delta_ns == 0) {
		tache(&last, &now, &delta_s, &delta_ns);
		return;
	}

	tache(&last, &now, &delta_s, &delta_ns);

	printf("Temps écoulé = %ld s et %9ld ns\n", delta_s, delta_ns);
    return;
}

int main(int argc, char const *argv[])
{
	extern void handle_sig1();
	int tmp;
	
	/* Scheduler boilerplate */
	pid_t ppid = getpid();
	
	struct sched_param sp;
	sp.sched_priority = PRIO;

	tmp = sched_setscheduler(ppid, SCHED_FIFO, &sp);
	// syscheck(tmp, "sched_setscheduler failure in main");
	
	/* Signal handling */
    struct sigaction sa1; 
    sa1.sa_handler = handle_sig1;
    sigemptyset(&sa1.sa_mask);
	// sigaddset(&sa1.sa_mask, SIG1);
    sa1.sa_flags = 0;
    
    tmp = sigaction(SIG1, &sa1, NULL);
    syscheck(tmp, "sigaction sig1 failure in main");

	/* Timer-making boilerplate */	
	timer_t timer_id = 0;

	struct sigevent sigev;
	memset(&sigev, 0, sizeof(struct sigevent));
	sigev.sigev_notify = SIGEV_SIGNAL;
	sigev.sigev_signo  = SIG1;
	sigev.sigev_value.sival_ptr = &timer_id;

	sigset_t mask;
	tmp = sigemptyset(&mask);
	syscheck(tmp, "wut");
	tmp = sigaddset(&mask, SIG1);
	syscheck(tmp, "sigaddset failure in main");

	#ifdef block
	/* Block timer temporarily */
	tmp = sigprocmask(SIG_SETMASK, &mask, NULL);
	syscheck(tmp, "sigprocmask failure in main");
	#endif

	/* Timer create */
	tmp = timer_create(CLOCK_REALTIME, &sigev, &timer_id);
	syscheck(tmp, "timer_create failure in main loop");
	DEBUG("Timer ID is 0x%lx\n", (long) timer_id);

	/* Clock resolution */
	struct timespec timeres;
	clock_getres(CLOCK_REALTIME, &timeres);
	DEBUG("Clock res: %ld ns\n", timeres.tv_nsec);

	/* Timeval */
	struct itimerspec its;
	memset(&its, 0, sizeof(struct itimerspec));
	its.it_value.tv_sec = 1;
	its.it_interval.tv_sec = its.it_value.tv_sec;

	/* Behavioral code */
	int i = 0;

	sigset_t empty_mask;
	tmp = sigfillset(&empty_mask);
	syscheck(tmp, "wut");
	tmp = sigdelset(&empty_mask, SIG1);
	syscheck(tmp, "sigaddset failure in main");
	
	do
	{
		tmp = timer_settime(timer_id, 0, &its, NULL);
		// DEBUG("%d - %d %d \n", errno, EFAULT, EINVAL);
		syscheck(tmp, "timer_settime failure in main loop");
		sigsuspend(&empty_mask);
		i++;
	} while (i < 10);

	#ifdef block
	tmp = sigprocmask(SIG_UNBLOCK, &mask, NULL);
	syscheck(tmp, "sigprocmask unblocking failure in main");
	#endif

	pid_t pid = fork();
	switch (pid) {
		case 0:
			// Child
			DEBUG(" Parent is (%d)\n", ppid);

			exit(0);
		case -1:
			// Fork error
			exit(2);
		default:
			// Parent
			exit(0);
	}


	return 0;
}
