#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sched.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stddef.h>

#include "ll.h"

/*
      \.  -   -  .                                          ,.  _~-.,           
     '          _ , -`.                                  ~'`_ \/,_. \_
   '        _,'     _,'                                 / ,"_>@`,__`~.)         
  '      ,-'      _/           ,                        | |  @@@@'  ",! .       
 '    ,-' \     _/       __,,,,,,                       |/   ^^@     .!  \      
'   ,'     \  _'        ////é è                         `' .^^^     ,'    '     
'  '       _\'          \\\'  >                          .^^^   .          \    
' ,    _,-'  \    _______ ) _^                          .^^^       '  .     \   
\,_,--'%%%%%%%\   \\__ __/ /_\                .,.,.     ^^^             ` .   .,
%%%%%%%%%%%%%%%\   \\+/  \ /  \&&&&&&&&&&&&&&&&&&&&&,  ,^^^^.  . ._ ..__ _  .'  
                \   \\| '    ' \%%%%%%%%%%%%%%%%%%%%%%%^^^^^^%%&&;_,.-=~'`^`'~=-
        RIP      \   \\ /\      \%%%%%%%%%%%%%%&&&&&%%%%%%%%%%%%%%%%%%&&;,.-=~'`
                  \   \\ \___`-.________&&&&&&&%%%%%&&&&&&&&&&&%%%%&&&_,.;^`'~=-
   Ci gissent les  \   \\__,( \_____  - \%%%%%%%%%%%%%%&&&&&&&&&-=~'`^`'~=-.,__,
     vacances de    \   \`---\/\----), ) \%%%%%%%%%%%&&&&
   monsieur Clauss   \   ||+=+=+=+=/  /\  \   _,.-=~'`^`'~=-.,__,.-=~'`^`'~=-.,_
                      \  ||________| /\ `. \  
%%%%%%%%%%%%%%%%%%%%%%%\ ||------- )/-\\  ) \ ~`'^`'~=-.,__,.-=~'`^`'~=-.,__,.-=
                        \||      ,'/   \\  \ \       / /         '-`            

                        
                            ^ (C'est vous)
*/

#define true 1
#define false 0
#define RESET   "\033[0m"
#define RED     "\033[31m"
#define CYAN    "\x1B[36m"

#define ERR(format, ...) {                                                     \
	fprintf(stderr, RED);                                                      \
	fprintf(stderr, "ERROR: ");                                                \
	fprintf(stderr, format, __VA_ARGS__);                                      \
	fprintf(stderr, RESET);                                                    \
	fflush(stderr);                                                            \
}                                                                              \

#define DEBUG(format, ...) {                                                   \
	fprintf(stderr, CYAN);                                                     \
	fprintf(stderr, "[DEBUG]");                                                \
	fprintf(stderr, RESET);                                                    \
	fprintf(stderr, format, __VA_ARGS__);                                      \
	fflush(stderr);                                                            \
}                                                                              \


/**
 * @brief Checks an assertion and prints the standard system primitives errors
 * @details Checks an assertion 
 * If the check fails, it tries to print the message along with a posix error 
 * message
 * 
 * If there are none, it simply prints the message alone.
 * 
 * @param R Will be compared to -1
 * @param G Message
 */

static inline void syscheck(int expr, const char* msg) {
	if ( expr == -1 ) {
		if (errno) {
			ERR("Assertion failed: %s (%s)\n",
				(char*) strerror(errno), msg);
		} else {
			ERR("Assertion failed (%s)\n", msg);
		}
		exit(1);
	}
}

static inline void check(int expr, const char* msg) {
	if (expr == 0) {
		ERR("Assertion failed: %s", msg);
		exit(1);
	}
}

int waitfor(int child) {
	// Wait for the child to actually terminate
	int status;
	pid_t ended_child;
	do {
		ended_child = waitpid(child, &status, 0);
	} while (WIFEXITED(status) != 1 && ended_child != child);

	// Print a debug diagnostic
	int t0,t1,t2,t3,t4,t5;
	DEBUG(" Child diagnostic: \n\
	WIFEXITED   => %d\n\
	WEXITSTATUS => %d\n\
	WIFSIGNALED => %d\n\
	WTERMSIG    => %d\n\
	WIFSTOPPED  => %d\n\
	WSTOPSIG    => %d\n\
",
		t0=WIFEXITED(status),
		t1=WEXITSTATUS(status),
		t2=WIFSIGNALED(status),
		t3=WTERMSIG(status),
		t4=WIFSTOPPED(status),
		t5=WSTOPSIG(status)
	);

	// Just WIFEXITED is the normal behavior.
	if (t0 && !(t1 | t2 | t3 | t4 | t5)) {
		DEBUG("  Results => OK (%d)\n", ended_child);
	}
	else {
		DEBUG("  Results => Something happened ! (%d)\n", ended_child);
	}


	return 1;
}



/*
Ecrire un processus exécutant une tâche de période 1 seconde, et qui consiste à afficher, à
chaque fois qu’elle est appelée, le nombre de secondes écoulées depuis son lancement.
Ce processus devra être ordonnancé selon la politique SCHED_FIFO avec une priorité = 17,
et devra utiliser un timer périodique POSIX4 dont l'expiration sera signalée par SIGRTMIN.
Ce timer expirera la première fois à 1 seconde. Le processus attendra la prochaine expiration
du timer avec sigsuspend. La tâche effectuera son affichage à chaque expiration du timer. 
On limitera l'exécution du processus à 30 secondes. 
Type de résultat attendu :

Temps écoulé = 1 s et 1745000 ns
Temps écoulé = 2 s et 2593000 ns
Temps écoulé = 3 s et 3441000 ns
Temps écoulé = 4 s et 4288000 ns
Temps écoulé = 5 s et 5146000 ns
*/


/*
 *	Actual code
 */

struct msg {
	char* str;
};

struct msg *msg_list = NULL;

void defered_printf(const char* format, ...) {
	if (msg_list == NULL) {
		msg_list = ll_new(msg_list);
	}

	va_list args;
	va_start(args, format);
	
	sprintf(msg_list->str, format, args);
}

void *printing_thread() {
	while (true) {
		pause();
	}
	return NULL;
}

void tache(int sig) {
	static struct timespec last = {0, 0};
	struct timespec now;
	int tmp; 

	tmp = clock_gettime(CLOCK_REALTIME, &now);

	syscheck(tmp, "clock failure in main");
	printf("%ld\n", now.tv_nsec - last.tv_nsec);

	last = now;
}

int main(int argc, char const *argv[])
{
	int tmp;
	/* Behavioral code */
	pid_t ppid = getpid();

	pthread_t thread;
	tmp = pthread_create(&thread, NULL, printing_thread, NULL);
	syscheck(tmp, "pthread_create failure in main");

	struct timespec timeres;
	clock_getres(CLOCK_REALTIME, &timeres);
	printf("%ld\n", timeres.tv_nsec);

	tache(0);
	tache(0);
	tache(0);
	tache(0);
	tache(0);
	tache(0);
	tache(0);

	pid_t pid = fork();
	switch (pid) {
		case 0:
			// Child
			DEBUG("Parent is (%d)\n", ppid);

			exit(0);
		case -1:
			// Fork error
			exit(2);
		default:
			// Parent
			exit(0);
	}


	return 0;
}
