## Ex4

#### Performances

Il semble plus facile pour mon programme d'envoyer une grande quantité de signaux temps réels, mais surtout, ils atteignent mon enfant beaucoup plus souvent !

#### Débit

Sur turing, SIGRTMAX = 64 et SIGRTMIN = 32

On peut donc faire transiter 32 bits par signal, pour environ 500 000 signaux par seconde, soit 2 000 000 d'octets par seconde (2 mo). 

#### (Détails)

Résultats avec les signaux temps réel:

```
Child is 7481
Parent is 7473
Sending signals to 7473
SENT: SIG1 (913 385)
GOT : SIG1 (913 385)
Sending signals to 7481
SENT: SIG1 (955 970)
GOT : SIG1 (955 970)

  Child diagnostic:
        WIFEXITED   => 1
        WEXITSTATUS => 0
        WIFSIGNALED => 0
        WTERMSIG    => 0
        WIFSTOPPED  => 0
        WSTOPSIG    => 0
  Results => OK (7481)
Exit code: 0
```

Résultats sans les signaux temps réel:

```
Child is 10740
Parent is 10739
Sending signals to 10739
SENT: SIG1 (644 363)
GOT : SIG1 (156 879)
Sending signals to 10740
SENT: SIG1 (977 298)
GOT : SIG1 (192 673)

  Child diagnostic:
        WIFEXITED   => 1
        WEXITSTATUS => 0
        WIFSIGNALED => 0
        WTERMSIG    => 0
        WIFSTOPPED  => 0
        WSTOPSIG    => 0
  Results => OK (10740)
Exit code: 0
```

## Ex5

Avec ma queue de signaux temps réel, je reçois de temps en temps une erreur:

```
EAGAIN (Resource temporarily unavailable)
```

Cela est du au dépassement du nombre de signaux autorisés dans la queue par mon programme

J'ai demandé à mon programme de me montrer les valeurs des variables "système" rlim_cur (limite souple) et rlim_max (limite stricte)

```
  struct rlimit rlim;
  getrlimit(RLIMIT_SIGPENDING, &rlim);
  DEBUG("rlim_cur: %'lu\n", rlim.rlim_cur);
  DEBUG("rlim_max: %'lu\n", rlim.rlim_max);
```

La limite douce est une valeur que je peux modifier, et correspond au nombre de signaux que le noyau m'autorise (basé sur l'UID réel de mon processus) à mettre dans la queue. 

La limite stricte est le plafond théorique de ma limite souple si je ne lance pas mon processus avec les droits d'administrateur.

Dans mon cas (Sur Turing) les deux sont égales; je peux cependant diminuer ma limite souple.

rlim_cur: 772 460
rlim_max: 772 460

#### (Details)

Avec sigqueue (10s)

```
rlim_cur: 772 460
rlim_max: 772 460
Child is (10928)
Parent is (10927)
Sending signals to (10927)
SENT: SIG1 => 6 562 172 (2 603 682 not sent)
GOT : SIG1 => 6 562 172
Sending signals to (10928)
SENT: SIG1 => 6 653 628 (3 015 357 not sent)
GOT : SIG1 => 6 653 628

  Child diagnostic:
        WIFEXITED   => 1
        WEXITSTATUS => 0
        WIFSIGNALED => 0
        WTERMSIG    => 0
        WIFSTOPPED  => 0
        WSTOPSIG    => 0
  Results => OK (10928)
Exit code: 0
```

Sans sigqueue (10s)

```
rlim_cur: 772 460
rlim_max: 772 460
Child is (31721)
Parent is (31720)
Sending signals to (31720)
SENT: SIG1 => 11 668 813 (0 not sent)
GOT : SIG1 => 6 582 572
Sending signals to (31721)
SENT: SIG1 => 11 964 217 (0 not sent)
GOT : SIG1 => 6 609 038

  Child diagnostic:
        WIFEXITED   => 1
        WEXITSTATUS => 0
        WIFSIGNALED => 0
        WTERMSIG    => 0
        WIFSTOPPED  => 0
        WSTOPSIG    => 0
  Results => OK (31721)
Exit code: 0

```

