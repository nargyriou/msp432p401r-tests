#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <locale.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <errno.h>

#define EXECUTION_TIME_LIMIT 10
#define SIG1    SIGRTMIN
#define SIG2    (SIGRTMIN+1)
#define RESET   "\033[0m"
#define RED     "\033[31m"      /* Red */
#define ERR(format, ...) {                                                     \
	fprintf(stderr, RED);                                                      \
	fprintf(stderr, format, __VA_ARGS__);                                      \
	fprintf(stderr, RESET);                                                    \
}                                                                              \

/**
 * @brief Checks an assertion and prints the standard system primitives errors
 * @details Checks an assertion 
 * If the check fails, it tries to print the message along with a posix error 
 * message
 * 
 * If there are none, it simply prints the message alone.
 * 
 * @param R Will be compared to -1
 * @param G Message
 */
#define CHECK(EXPR, MSG) {                                                     \
	if ( (EXPR) == -1 ) {                                                      \
		if ( (errno) ) {                                                       \
			ERR("Assertion failed: %s (%s)",                                   \
				(char*) strerror(errno), (MSG) );                              \
		} else {                                                               \
			ERR("Assertion failed (%s)", (MSG) );                              \
		}                                                                      \
		exit(1);                                                               \
	}                                                                          \
}

#define DEBUG(format, ...) printf(format, __VA_ARGS__)

int PARENT_FINISHED;
int SIG1_COUNT;
/**
 * @brief Usage interne
 * @details Si l'argument PID présent, set le pid de l'enfant dans une variable statique pour une 
 * utilisation plus tard par les diverses fonctions handler 
 * 
 * Sinon, elle envoie un signal au PID sélectionné précédemment
 * 
 * Sert à éviter trop de variables globales (mais finalement j'ai abandonné)
 */
int _sigusr_to_child(int new_pid, int sig) {
	static int pid = -1;

	if (new_pid != 0) {
		pid = new_pid;
		DEBUG("Sending signals to (%d)\n", pid);
	}

	if (pid != -1 && sig > 0) {
		union sigval v;
		v.sival_int = 0;
		// int tmp = kill(pid, sig);
		int tmp = sigqueue(pid, sig, v);

		if (tmp == -1) {
			// Trop de messages :')
			// ERR("Sigqueue failed! (%s)\n", (char*) strerror(errno));
			return -1;
		}
	}

	return 0;
}

/**
 * @brief Définit l'enfant à qui on enverra des signaux par la suite
 */
void child_set(int new_pid) {
	_sigusr_to_child(new_pid, 0);
}

/**
 * @brief Envoie un signal à l'enfant défini par child_set
 */
int child_send(int sig) {
	return _sigusr_to_child(0, sig);
}

void handle_sigalarm(int sig)
{
	PARENT_FINISHED = 1;
	return;
}

void handle_sig1(int sig) {
	++SIG1_COUNT;
	return;
}

void handle_sig2(int sig) {
	PARENT_FINISHED = 1;
	return;
}

void be_a_child() {
	PARENT_FINISHED = 0;
	SIG1_COUNT = 0;

	while (PARENT_FINISHED == 0) { // set by setusr2
		//sigsuspend(&sig1_action.sa_mask);
		pause();
	}
	DEBUG("GOT : SIG1 => %'d\n", SIG1_COUNT);
	fflush(stdout);
}

void be_a_parent(int pid, int wait_time) {
	PARENT_FINISHED = 0;
	int nsig1_tries = 0;
	int nsig1_sent = 0;

	child_set(pid);
	alarm(wait_time);
	while (PARENT_FINISHED == 0) {
		++nsig1_tries;
		if (child_send(SIG1) != -1) {
			++nsig1_sent;
		}
	}
	DEBUG("SENT: SIG1 => %'d (%'d not sent)\n", 
		nsig1_sent, nsig1_tries - nsig1_sent);

	while (child_send(SIG2) == -1){
	};
}

int waitfor(int child) {
	// Wait for the child to actually terminate
	int status;
	pid_t ended_child;
	do {
		ended_child = waitpid(child, &status, 0);
	} while (WIFEXITED(status) != 1 && ended_child != child);

	// Print a debug diagnostic
	int t0,t1,t2,t3,t4,t5;
	DEBUG("\n  Child diagnostic: \n\
	WIFEXITED   => %d\n\
	WEXITSTATUS => %d\n\
	WIFSIGNALED => %d\n\
	WTERMSIG    => %d\n\
	WIFSTOPPED  => %d\n\
	WSTOPSIG    => %d\n\
",
		t0=WIFEXITED(status),
		t1=WEXITSTATUS(status),
		t2=WIFSIGNALED(status),
		t3=WTERMSIG(status),
		t4=WIFSTOPPED(status),
		t5=WSTOPSIG(status)
	);

	// Just WIFEXITED is the normal behavior.
	if (t0 && !(t1 | t2 | t3 | t4 | t5))
		DEBUG("  Results => OK (%d)\n", ended_child);
	else
		DEBUG("  Results => Something happened ! (%d)\n", ended_child);


	return 1;
}

int main(int argc, char const *argv[])
{
    extern void handle_sig1(), alarm_handler(), handle_sig2();

    // Make number display more pleasant
	setlocale(LC_NUMERIC, "");

	// Handle SIGALRM with handle_sigalarm()
	struct sigaction sa; 
	sa.sa_handler = handle_sigalarm;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	CHECK(sigaction(SIGALRM, &sa, NULL), "Sigaction for alarm");

	// Handle SIG1 with handle_sig1()
	struct sigaction su1; 
	su1.sa_handler = handle_sig1;
	sigemptyset(&su1.sa_mask);
	su1.sa_flags = 0;
	
	CHECK(sigaction(SIG1, &su1, NULL), "Sigaction sig1");

	// Handle SIG2 with handle_sig2()
	struct rlimit rlim;
	struct sigaction su2;
	su2.sa_handler = handle_sig2;
	sigemptyset(&su2.sa_mask);
	su2.sa_flags = 0;
	
	CHECK(sigaction(SIG2, &su2, NULL), "Sigaction sig2");

	pid_t ppid = getpid();

	pid_t pid = fork();
	switch (pid) {
		case 0:
			// Child
			DEBUG("Parent is (%d)\n", ppid);

			be_a_parent(ppid, EXECUTION_TIME_LIMIT);
			be_a_child();

			exit(0);
		case -1:
			exit(2);
		default:
			// Parent
			getrlimit(RLIMIT_SIGPENDING, &rlim);
			DEBUG("rlim_cur: %'lu\n", rlim.rlim_cur);
			DEBUG("rlim_max: %'lu\n", rlim.rlim_max);
			DEBUG("Child is (%d)\n", pid);

			be_a_child();
			be_a_parent(pid, EXECUTION_TIME_LIMIT);
			waitfor(pid);

			exit(0);
	}

	return 3;
}
