#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <locale.h>
#include <sys/wait.h>
#include <errno.h>

#define RESET   "\033[0m"
#define RED     "\033[31m"      /* Red */
#define ERR(format, ...) {                \
	fprintf(stderr, RED);                 \
	fprintf(stderr, format, __VA_ARGS__); \
	fprintf(stderr, RESET);               \
}                                         \

/**
 * @brief Checks an assertion and prints the standard system primitives errors
 * @details Checks an assertion 
 * If the check fails, it tries to print the message along with a posix error 
 * message
 * 
 * If there are none, it simply prints the message alone.
 * 
 * @param R Will be compared to -1
 * @param G Message
 */
#define CHECK(EXPR, MSG) {                                                     \
	if ( (EXPR) == -1 ) {                                                      \
		if ( (errno) ) {                                                       \
			ERR("Assertion failed: %s (%s)",                                   \
				(char*) strerror(errno), (MSG) );                              \
		} else {                                                               \
			ERR("Assertion failed (%s)", (MSG) );                              \
		}                                                                      \
		exit(1);                                                               \
	}                                                                          \
}

#define DEBUG(format, ...) printf(format, __VA_ARGS__)

int PARENT_FINISHED;
int SIGUSR1_COUNT;
/**
 * @brief Usage interne
 * @details Set le pid de l'enfant pour plus tard ou envoie un signal, 
 * ou les deux en même temps
 */
void _sigusr_to_child(int new_pid, int sig) {
	static int pid = -1;

	if (new_pid != 0) {
		pid = new_pid;
		DEBUG("Sending signals to %d\n", pid);
	}

	if (pid != -1 && sig > 0) {
		kill(pid, sig);
	}
}

/**
 * @brief Définit l'enfant à qui on enverra des signaux par la suite
 */
void child_set(int new_pid) {
	_sigusr_to_child(new_pid, 0);
}

/**
 * @brief Envoie un signal à l'enfant défini par child_set
 */
void child_send(int sig) {
	_sigusr_to_child(0, sig);
}

void handle_sigalarm(int sig)
{
	PARENT_FINISHED = 1;
	return;
}

void handle_sigusr1(int sig) {
	++SIGUSR1_COUNT;
	return;
}

void handle_sigusr2(int sig) {
	PARENT_FINISHED = 1;
	return;
}

void be_a_child() {
	PARENT_FINISHED = 0;
	SIGUSR1_COUNT = 0;

	while (PARENT_FINISHED == 0) { // set by setusr2
		//sigsuspend(&sigusr1_action.sa_mask);
		pause();
	}
	DEBUG("GOT : SIGUSR1 (%'d)\n", SIGUSR1_COUNT);
	fflush(stdout);
}

void be_a_parent(int pid, int wait_time) {
	PARENT_FINISHED = 0;
	int nsigusr1_sent = 0;

	child_set(pid);
	alarm(wait_time);
	while (PARENT_FINISHED == 0) {
		child_send(SIGUSR1);
		++nsigusr1_sent;
	}
	DEBUG("SENT: SIGUSR1 (%'d)\n", nsigusr1_sent);
	child_send(SIGUSR2);
}

int waitfor(int child) {
	// Wait for the child to actually terminate
	int status;
	pid_t ended_child;
	do {
		ended_child = waitpid(child, &status, 0);
	} while (WIFEXITED(status) != 1 && ended_child != child);

	// Print a debug diagnostic
	DEBUG("\n  Child diagnostic: \n\
	WIFEXITED   => %d\n\
	WEXITSTATUS => %d\n\
	WIFSIGNALED => %d\n\
	WTERMSIG    => %d\n\
	WIFSTOPPED  => %d\n\
	WSTOPSIG    => %d\n\
",
		WIFEXITED(status), 
		WEXITSTATUS(status),
		WIFSIGNALED(status),
		WTERMSIG(status),
		WIFSTOPPED(status),
		WSTOPSIG(status)
	);

	return 1;
}

int main(int argc, char const *argv[])
{
    extern void handle_sigusr1(), alarm_handler(), handle_sigusr2();

    // Make number display more pleasant
	setlocale(LC_NUMERIC, "");

	// Handle SIGALRM with handle_sigalarm()
	struct sigaction sa; 
	sa.sa_handler = handle_sigalarm;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	CHECK(sigaction(SIGALRM, &sa, NULL), "Sigaction for alarm");

	// Handle SIGUSR1 with handle_sigusr1()
	struct sigaction su1; 
	su1.sa_handler = handle_sigusr1;
	sigemptyset(&su1.sa_mask);
	su1.sa_flags = 0;
	
	CHECK(sigaction(SIGUSR1, &su1, NULL), "Sigaction sigusr1");

	// Handle SIGUSR2 with handle_sigusr2()
	struct sigaction su2;
	su2.sa_handler = handle_sigusr2;
	sigemptyset(&su2.sa_mask);
	su2.sa_flags = 0;
	
	CHECK(sigaction(SIGUSR2, &su2, NULL), "Sigaction sigusr2");

	pid_t ppid = getpid();

	pid_t pid = fork();
	switch (pid) {
		case 0:
			// Child
			DEBUG("Parent is %d\n", ppid);

			be_a_parent(ppid, 1);
			be_a_child();

			exit(0);
		case -1:
			exit(2);
		default:
			// Parent
			DEBUG("Child is %d\n", pid);

			be_a_child();
			be_a_parent(pid, 1);
			waitfor(pid);

			exit(0);
	}

	return 3;
}
