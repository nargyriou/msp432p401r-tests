
#include <ti/devices/msp432p4xx/driverlib/driverlib.h>
#define PORT INT_PORT3
#define BTN_PORT GPIO_PORT_P3 
#define BTN_PIN BTN_PORT, GPIO_PIN5


void intHandler(void) {
  GPIO_toggleOutputOnPin(GPIO_PORT_P1,GPIO_PIN0);
  GPIO_clearInterruptFlag(BTN_PIN);
}

void setup() {
  // stop watchdog timer
  // WDT_A_hold(WDT_A_BASE);
  WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;
  Serial.begin(9600);
  
  // Red LED
  GPIO_setAsOutputPin(
      GPIO_PORT_P1,
      GPIO_PIN0
  );
  GPIO_toggleOutputOnPin(GPIO_PORT_P1,GPIO_PIN0);
  
  /*
  Interrupt_registerInterrupt(PORT, &intHandler);
  Interrupt_enableMaster();
  Interrupt_enableInterrupt(PORT);
  */

  // Btn Interrupt
  GPIO_setAsInputPinWithPullUpResistor(BTN_PIN);
  
  GPIO_interruptEdgeSelect(BTN_PIN, GPIO_HIGH_TO_LOW_TRANSITION);
  GPIO_registerInterrupt(BTN_PORT, intHandler);
  Interrupt_setPriority(INT_PORT3, 0xC0);

  
  Serial.println(GPIO_getEnabledInterruptStatus(BTN_PORT));
  Serial.println(GPIO_PIN5);

  GPIO_enableInterrupt(BTN_PIN);
  Interrupt_enableMaster();
  Serial.println("Registered");

  // NVIC_EnableIRQ(PORT3_IRQn);
  // NVIC_ISER0 = 1 << ((INT_PORT1 - 16) &31);
  for (;;){
  }
  
}

void loop() {
}
